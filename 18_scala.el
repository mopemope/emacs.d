(use-package ensime
  :ensure t
  :defer t)

(use-package scala-mode
  :ensure t
  :defer t
  :interpreter
  ("scala" . scala-mode))

(use-package sbt-mode
  :ensure t
  :defer t
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map))
