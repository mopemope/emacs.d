;;;
;;; eglot, lsp-mode
;;;

(use-package lsp-mode
  :commands lsp
  :ensure t
  :init
  (setq lsp-inhibit-message t))

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(use-package company-lsp
  :defer t
  :ensure t
  :commands company-lsp
  :config
  (setq company-lsp-cache-candidates t
        company-lsp-async t))

(use-package dap-mode
  :ensure t
  :defer t
  :after lsp-mode)

(use-package eglot
  :commands (eglot eglot-ensure)
  :defer t
  :ensure t
  :config
  (add-to-list 'eglot-server-programs '(go-mode . ("bingo")))
  (add-to-list 'eglot-server-programs '(vue-mode . ("vls"))))


