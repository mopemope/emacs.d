;;;
;;; go-mode
;;;

(defun gb--gopath ()
  (let* ((d (locate-dominating-file buffer-file-name "src"))
         (vendor (concat d (file-name-as-directory "vendor"))))
    (if (and d
             (file-exists-p vendor))
        (list (concat (file-name-as-directory d) "src")
              (concat (file-name-as-directory vendor) "src")))))

(defun gb-set-project ()
  (interactive)
  (let* ((gopath (gb--gopath))
         (path (mapconcat
                (lambda (el) (file-truename el))
                gopath
                ":")))
    (message "Set GOPATH to %s" path)
    (setenv "GOPATH" path)))


(use-package go-mode
  :defer t
  :ensure t
  :mode ("\\.go$" . go-mode)
  :commands (go-mode)
  :hook
  (go-mode . (lambda ()
                    (lsp)
                    (yas-minor-mode)))
  :config
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save))

;; (use-package go-mode
;;   :defer t
;;   :ensure t
;;   :mode ("\\.go$" . go-mode)
;;   :commands (go-mode)
;;   :init
;;   (add-hook 'go-mode-hook (lambda ()
;;                             (dumb-jump-mode)
;;                             (go-eldoc-setup)
;;                             (smartparens-mode t)
;;                             (rainbow-delimiters-mode t)))
;;   :config
;;   (use-package golint
;;     :ensure t)
;;   (use-package go-eldoc
;;     :ensure t)
;;   (use-package company-go
;;     :ensure t)
;;   (use-package go-guru
;;     :ensure t)
;;   (setq godef-command "godef")
;;   (setq gofmt-command "goimports")
;;   (setq company-go-insert-arguments nil)
;;   (setq company-go-show-annotation t)
;;   (flycheck-mode t)
;;   (setq flycheck-check-syntax-automatically '(mode-enabled save))
;;   (setq go-guru-debug t)
;;   (smartparens-mode t)
;;   (add-to-list 'company-backends '(company-go :with company-dabbrev-code))
;;   (setq company-transformers '(company-sort-by-backend-importance))
;;   (add-hook 'before-save-hook 'gofmt-before-save))

;;;###autoload (autoload 'go-format-buffer "go-format" nil t)
;;;###autoload (autoload 'go-format-on-save-mode "go-format" nil t)
(reformatter-define go-format
  :program "go"
  :args (list "fmt")
  :group 'go
  :lighter 'GoFmt)
