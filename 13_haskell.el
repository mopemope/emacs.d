;;;
;;; haskell-mode
;;;

(use-package haskell-mode
  :ensure t
  :defer t
  :mode ("\\.hs$" . haskell-mode)
  :hook (haskell-mode-hook . (lambda()
                               (intero-mode t)
                               (flycheck-mode t)
                               (hindent-mode t)
                               (smartparens-mode t)
                               (rainbow-delimiters-mode t)))
  :commands (haskell-mode)
  :config
  (use-package cmm-mode
    :ensure t)
  (use-package company
    :ensure t)
  (use-package haskell-snippets
    :ensure t)
  (use-package hindent
    :ensure t)
  (use-package hlint-refactor
    :ensure t)
  (use-package intero
    :ensure t
    :config
    (flycheck-add-next-checker 'intero '(warning . haskell-hlint)))
  (use-package flycheck-haskell
    :ensure t
    :commands flycheck-haskell-configure
    :hook
    ((flycheck-mode-hook . flycheck-haskell-configure)))
  (setq hindent-reformat-buffer-on-save t))
