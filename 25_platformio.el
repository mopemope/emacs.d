(use-package platformio-mode
  :ensure t
  :defer t
  :init
  (add-hook 'c-mode-hook 'platformio-conditionally-enable)
  (add-hook 'c++-mode-hook 'platformio-conditionally-enable)
  :commands (platformio-mode platformio-conditionally-enable)
  :config
  (use-package projectile
    :ensure t)
  (use-package irony
    :ensure t)
  (use-package company-irony
    :ensure t)
  (use-package irony-eldoc
    :ensure t)
  (use-package flycheck-irony
    :ensure t)

  (setq indent-tabs-mode nil)
  (setq tab-width 2)
  (setq c-basic-offset 2)
  (rainbow-delimiters-mode t)
  (smartparens-mode t)
  (dumb-jump-mode t))
