(require 'cl-lib)
(require 'color)

;;
;; basic settings
;;

(use-package s
  :defer t
  :ensure t
  :commands (s-trim
             s-blank-str?
             s-snake-case
             s-lower-camel-case
             s-word-initials
             s-contains?))

(use-package server
  :ensure t
  :config
  (unless (server-running-p)
    (server-start))
  (when (display-graphic-p)
    (add-to-list 'default-frame-alist '(font . "Ricty Diminished 14"))
    (set-face-attribute 'default nil :font "Ricty Diminished 14")))

(global-display-line-numbers-mode)

(use-package whitespace
  :ensure t
  :config
  (setq whitespace-style '(space-mark tab-mark face spaces trailing))
  (setq whitespace-display-mappings
        '(
          ;; (space-mark   ?\     [?\u00B7]     [?.]) ; space - centered dot
          (space-mark   ?\xA0  [?\u00A4]     [?_]) ; hard space - currency
          (space-mark   ?\x8A0 [?\x8A4]      [?_]) ; hard space - currency
          (space-mark   ?\x920 [?\x924]      [?_]) ; hard space - currency
          (space-mark   ?\xE20 [?\xE24]      [?_]) ; hard space - currency
          (space-mark   ?\xF20 [?\xF24]      [?_]) ; hard space - currency
          (space-mark ?\u3000 [?\u25a1] [?_ ?_]) ; full-width-space - square
          (tab-mark     ?\t    [?\u00BB ?\t] [?\\ ?\t]) ; tab - left quote mark
          ))
  (setq whitespace-space-regexp "\\(\u3000+\\)")
  (set-face-foreground 'whitespace-space "cyan")
  (set-face-background 'whitespace-space 'nil)
  (set-face-underline  'whitespace-trailing t)
  (set-face-foreground 'whitespace-trailing "cyan")
  (set-face-background 'whitespace-trailing 'nil))


(setq debug-on-error nil)
;; (setq debug-on-error t)

(require 'mozc)
(set-language-environment "Japanese")
(setq default-input-method "japanese-mozc")
(global-set-key [zenkaku-hankaku] 'toggle-input-method)
(global-set-key [henkan]
                (lambda ()
                  (interactive)
                  (when (null current-input-method)
                    (toggle-input-method))))
(global-set-key [muhenkan]
                (lambda ()
                  (interactive)
                  (deactivate-input-method)))

(defadvice mozc-handle-event (around intercept-keys (event))
  "Intercept keys muhenkan and zenkaku-hankaku, before passing keys
to mozc-server (which the function mozc-handle-event does), to
properly disable mozc-mode."
  (if (member event (list 'zenkaku-hankaku 'muhenkan))
      (progn
        (mozc-clean-up-session)
        (toggle-input-method))
    (progn
      ad-do-it)))
(ad-activate 'mozc-handle-event)
(global-unset-key (kbd "C-\\"))

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(global-set-key (kbd "C-h") 'delete-backward-char)
(column-number-mode 1)
(cua-mode t)
(setq cua-enable-cua-keys nil)
(show-paren-mode t)

(setq-default fill-column 120)
(setq-default c-basic-offset 2
              tab-width 2
              indent-tabs-mode nil)

(setq vc-follow-symlinks t)
(setq auto-revert-check-vc-info t)
(setq inhibit-splash-screen t)

(setq make-backup-files nil)
(setq auto-save-default nil)
(setq auto-save-list-file-prefix nil)
(setq create-lockfiles nil)

(setq recentf-save-file "~/.emacs.d/.recentf")
(setq recentf-max-saved-items 10000)

(savehist-mode t)
(setq history-length 1000)
(setq messagep-log-max 10000)

(defun beginning-of-indented-line (current-point)
  (interactive "d")
  (if (string-match
       "^[ \t]+$"
       (save-excursion
         (buffer-substring-no-properties
          (progn (beginning-of-line) (point))
          current-point)))
      (beginning-of-line)
    (back-to-indentation)))

(defun beginning-of-visual-indented-line (current-point)
  (interactive "d")
  (let ((vhead-pos (save-excursion (progn (beginning-of-visual-line) (point))))
        (head-pos (save-excursion (progn (beginning-of-line) (point)))))
    (cond
     ((eq vhead-pos head-pos)
      (if (string-match
           "^[ \t]+$"
           (buffer-substring-no-properties vhead-pos current-point))
          (beginning-of-visual-line)
        (back-to-indentation)))
     ((eq vhead-pos current-point)
      (backward-char)
      (beginning-of-visual-indented-line (point)))
     (t (beginning-of-visual-line)))))

(defun new-next-line (current-point)
  (interactive "d")
  (next-line)
  (beginning-of-visual-indented-line current-point))

(defun new-previous-line (current-point)
  (interactive "d")
  (previous-line)
  (beginning-of-visual-indented-line current-point))

(defun toggle-snakecase ()
  "Toggle between camelcase and underscore notation for the symbol at point."
  (interactive)
  (save-excursion
    (let* ((bounds (bounds-of-thing-at-point 'symbol))
           (str (thing-at-point 'symbol))
           (start (car bounds))
           (end (cdr bounds)))
      (if (or (s-contains? "_" str) (s-contains? "-" str))
          (replace-string str (s-lower-camel-case str) nil start end)
        (replace-string str (s-snake-case str) nil start end)))))

(defun word-initials ()
    "Toggle between camelcase and underscore notation for the symbol at point."
  (interactive)
  (save-excursion
    (let* ((bounds (bounds-of-thing-at-point 'symbol))
           (str (thing-at-point 'symbol))
           (start (car bounds))
           (end (cdr bounds)))
      (replace-string str (s-word-initials str) nil start end))))


(global-set-key (kbd "C-a") 'beginning-of-visual-indented-line)
(global-set-key (kbd "C-e") 'end-of-visual-line)

;;(global-set-key (kbd "C-i") 'word-initials)

;; (defun forward-to-word (arg)
;;   (interactive "^p")
;;   (or (re-search-forward "\\(\\W\\b\\|.$\\)" nil t arg)
;;       (goto-char (point-max))))

;; (defun backward-to-word (arg)
;;   (interactive "^p")
;;   (or (re-search-backward "\\(\\W\\b\\|.$\\)" nil t arg)
;;       (goto-char (point-min))))

(defun current-line ()
  (string-to-number (format-mode-line "%l")))

(defun forward-to-word (arg)
  (interactive "^p")
  (let ((c1 (point))
        (l1 (current-line))
        (end (line-end-position))
        (p (viper-forward-word arg))
        (c2 (point))
        (l2 (current-line)))
    (when (and (> l2 l1) (/= c1 end))
      (re-search-backward "\\(\\W\\b\\|.$\\)" nil t arg)
      (forward-char 1))))

(defun backward-to-word (arg)
  (interactive "^p")
  (let ((c1 (point))
        (l1 (current-line))
        (begin (line-beginning-position))
        (p (viper-backward-word arg))
        (c2 (point))
        (l2 (current-line)))
    (when (and (> l1 l2) (/= c1 begin))
      (end-of-line))))

(require 'viper-cmd)

(global-set-key (kbd "C-<right>") 'forward-to-word)
(global-set-key (kbd "C-<left>") 'backward-to-word)

;; (global-set-key (kbd "M-f") 'forward-to-word)
;; (global-set-key (kbd "M-b") 'backward-to-word)
;; (global-set-key (kbd "M-<right>") 'forward-to-word)
;; (global-set-key (kbd "M-<left>") 'backward-to-word)
;; (global-set-key (kbd "C-<right>") 'forward-to-word)
;; (global-set-key (kbd "C-<left>") 'backward-to-word)

(transient-mark-mode t)
(delete-selection-mode t)

(setq redisplay-dont-pause t)
(global-whitespace-mode t)

(setq frame-title-format "%b")
(menu-bar-mode -1)
(tool-bar-mode 0)

(setq truncate-lines t)
(setq truncate-partial-width-windows nil)
(setq kill-whole-line t)

(defun set-alpha (alpha-num)
  "set frame parameter 'alpha"
  (interactive "nAlpha: ")
  (set-frame-parameter nil 'alpha (cons alpha-num '(100))))

(defalias 'exit 'save-buffers-kill-emacs)

(defun prev-window ()
  (interactive)
  (other-window -1))

(global-set-key (kbd "s-<left>") 'prev-window)
(global-set-key (kbd "s-<right>") 'other-window)
;; (global-set-key (kbd "C-s-<left>") 'previous-buffer)
;; (global-set-key (kbd "C-s-<right>") 'next-buffer)
(global-set-key (kbd "C-|") 'split-window-horizontally)

(defun delete-word (arg)
  (interactive "p")
  (delete-region (point) (progn (viper-forward-word arg) (point))))

(defun backward-delete-word (arg)
  (interactive "p")
  (delete-word (- arg)))

(global-set-key (kbd "M-d") 'delete-word)
(global-set-key [C-backspace] 'backward-delete-word)

(use-package auto-sudoedit
  :ensure t
  :config
  (auto-sudoedit-mode 1))

(use-package smart-hungry-delete
  :ensure t
  :bind (("<backspace>" . smart-hungry-delete-backward-char)
		     ("C-d" . smart-hungry-delete-forward-char))
  :config (smart-hungry-delete-add-default-hooks))
