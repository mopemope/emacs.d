;;;
;;; clojure-mode
;;;

;; cider-repl-mode
;; key             binding
;; ---             -------

;; C-c             Prefix Command
;; TAB             cider-repl-tab
;; C-j             cider-repl-newline-and-indent
;; RET             cider-repl-return
;; C-x             Prefix Command
;; ESC             Prefix Command
;; ,               cider-repl-handle-shortcut
;; C-S-a           cider-repl-bol-mark
;; <C-down>        cider-repl-forward-input
;; <C-return>      cider-repl-closing-return
;;   (that binding is currently shadowed by another mode)
;; <C-up>          cider-repl-backward-input
;; <S-home>        cider-repl-bol-mark

;; C-x C-e         cider-eval-last-sexp

;; M-n             cider-repl-next-input
;; M-p             cider-repl-previous-input
;; M-r             cider-repl-previous-matching-input
;; M-s             cider-repl-next-matching-input

;; C-c C-b .. C-c C-c              cider-interrupt
;; C-c C-d         cider-doc-map
;; C-c RET         cider-macroexpand-1
;; C-c C-n         cider-repl-next-prompt
;; C-c C-o         cider-repl-clear-output
;; C-c C-p         cider-repl-previous-prompt
;; C-c C-q         cider-quit
;; C-c C-r         cider-eval-region
;; C-c C-u         cider-repl-kill-input
;; C-c C-x         cider-refresh
;; C-c C-z         cider-switch-to-last-clojure-buffer
;; C-c ESC         Prefix Command
;; C-c C-.         cider-find-ns

;; C-c M-.         cider-find-resource
;; C-c M-i         cider-inspect
;; C-c M-m         cider-macroexpand-all
;; C-c M-n         cider-repl-set-ns
;; C-c M-o         cider-repl-switch-to-other
;; C-c M-s         cider-selector
;; C-c M-t         Prefix Command

;; C-c M-t n       cider-toggle-trace-ns
;; C-c M-t v       cider-toggle-trace-var

;; C-c C-d C-a     cider-apropos
;; C-c C-d C-d     cider-doc
;; C-c C-d C-j     cider-javadoc
;; C-c C-d C-r     cider-grimoire
;; C-c C-d A       cider-apropos-documentation
;; C-c C-d a       cider-apropos
;; C-c C-d d       cider-doc
;; C-c C-d h       cider-grimoire-web
;; C-c C-d j       cider-javadoc
;; C-c C-d r       cider-grimoire


;; (defhydra hydra-clj-refactor-a (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (a)"
;;   ("i" cljr-add-import-to-ns "Add import")
;;   ("r" cljr-add-require-to-ns "Add require")
;;   ("u" cljr-add-use-to-ns "Add use")
;;   ("m" cljr-add-missing-libspec "Add missing libspec")
;;   ("p" cljr-add-project-dependency "Add project dependency")
;;   ("d" cljr-add-declaration "Add declaration for current top-level form")
;;   ("s" cljr-add-stubs "Add stubs for interface at point"))

;; (defhydra hydra-clj-refactor-c (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (c)"
;;   ("c" cljr-cycle-coll "Cycle surrounding collection type" :color red)
;;   ("i" cljr-cycle-if "Cycle between if and if-not")
;;   ("p" cljr-cycle-privacy "Cycle privacy of defn or def")
;;   ("n" cljr-clean-ns "Clean namespace form")
;;   ("t" cljr-cycle-thread "Cycle threading between -> and ->>. Also applies to versions like cond->"))

;; (defhydra hydra-clj-refactor-d (:exit t
;;                                       :columns 1
;;                                       :idle 1.0)
;;   "Clojure Refactor (d)"
;;   ("k" cljr-destructure-keys "Destructure keys"))

;; (defhydra hydra-clj-refactor-e (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (e)"
;;   ("c" cljr-extract-constant "Extract constant")
;;   ("d" cljr-extract-def "Extract def")
;;   ("f" cljr-extract-function "Extract function")
;;   ("l" cljr-expand-let "Expand let"))

;; (defhydra hydra-clj-refactor-f (:exit t
;;                                       :columns 1
;;                                       :idle 1.0)
;;   "Clojure Refactor (f)"
;;   ("e" cljr-create-fn-from-example "Create fn from example stub")
;;   ("u" cljr-find-usages "Find usages"))

;; (defhydra hydra-clj-refactor-h (:exit t
;;                                       :columns 1
;;                                       :idle 1.0)
;;   "Clojure Refactor (h)"
;;   ("d" cljr-hotload-dependency "Hotload dependency"))

;; (defhydra hydra-clj-refactor-i (:exit t
;;                                       :columns 1
;;                                       :idle 1.0)
;;   "Clojure Refactor (i)"
;;   ("l" cljr-introduce-let "Introduce let")
;;   ("s" cljr-inline-symbol "Inline symbol"))

;; (defhydra hydra-clj-refactor-m (:exit t
;;                                       :columns 1
;;                                       :idle 1.0)
;;   "Clojure Refactor (m)"
;;   ("f" cljr-move-form "Move form(s) to other ns, :refer functions")
;;   ("l" cljr-move-to-let "Move to let"))

;; (defhydra hydra-clj-refactor-p (:exit t
;;                                       :columns 1
;;                                       :idle 1.0)
;;   "Clojure Refactor (p)"
;;   ("c" cljr-project-clean-functions "Run project clean functions on project")
;;   ("f" cljr-promote-function "Promote function literal to fn, or fn to defn"))

;; (defhydra hydra-clj-refactor-r (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (r)"
;;   ("d" cljr-remove-debug-fns "Remove debug function invocations")
;;   ("D" cljr-reify-to-defrecord "Reify to defrecord")
;;   ("f" cljr-rename-file-or-dir "Rename file or dir updating any affected files")
;;   ("l" cljr-remove-let "Remove let, inline all variables and remove the let form")
;;   ("r" cljr-remove-unused-requires "Remove unused requires")
;;   ("s" cljr-rename-symbol "Rename symbol")
;;   ("u" cljr-replace-use "Replace use statements with equivalent require statements"))

;; (defhydra hydra-clj-refactor-s (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (s)"
;;   ("c" cljr-show-changelog "Show the changelog, to learn about recent changes")
;;   ("n" cljr-sort-ns "Sort the ns form. :use, :require and :import clauses are sorted")
;;   ("p" cljr-sort-project-dependencies "Sort project dependencies found in project.clj")
;;   ("r" cljr-stop-referring "Stop referring (removes :refer [...] from current require, fix references)"))

;; (defhydra hydra-clj-refactor-t (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (t)"
;;   ("d" cljr-toggle-debug-mode "Toggle debug mode")
;;   ("f" cljr-thread-first-all "Wrap in thread-first (->) and fully thread")
;;   ("h" cljr-thread "Thread another expression")
;;   ("l" cljr-thread-last-all "Wrap in thread-last (->>) and fully thread"))

;; (defhydra hydra-clj-refactor-u (:exit t
;;                                       :columns 2
;;                                       :idle 1.0)
;;   "Clojure Refactor (u)"
;;   ("a" cljr-unwind-all "Fully unwind a threaded expression")
;;   ("d" cljr-update-project-dependency "Update project dependency (lein only)")
;;   ("p" cljr-update-project-dependencies "Update project dependencies (lein only)")
;;   ("w" cljr-unwind "Unwind a threaded expression"))

(defun _exit ()
  (interactive)
  (if (display-graphic-p)
      (delete-frame)
  (exit)))

(defhydra hydra-cider-mode (:hint nil :exit t)
"
^Main^                        ^Eval^                           ^Other^
^^^^^^--------------------------------------------------------------------------------------
_c_: cider-connect            _b_: cider-eval-buffer           _a_: helm-do-ag
_j_: cider-jack-in            _n_: cider-eval-ns-form          _A_: helm-do-ag-project-root
_i_: cider-inspect            _r_: cider-eval-region           _l_: helm-ls-git-ls
_t_: cider-toggle-trace       _f_: cider-eval-file             _g_: magit-status
_d_: cider-doc                _N_: repl-set-ns
_E_: compilation-error

_q_: exit
"

  ("c" cider-connect)
  ("j" cider-jack-in)
  ("i" cider-inspect)
  ("t" cider-toggle-trace)
  ("d" cider-doc)
  ("E" cider-jump-to-compilation-error)

  ("b" cider-eval-buffer)
  ("n" cider-eval-ns-form)
  ("r" cider-eval-region)
  ("f" cider-eval-file)
  ("N" cider-repl-set-ns)
  ("e" cider-eval-last-sexp)

  ("g" magit-status)
  ("a" helm-do-ag)
  ("A" helm-do-ag-project-root)
  ("l" helm-ls-git-ls)

  ("q" _exit)
  ("z" nil "leave"))

(defhydra hydra-cider-repl-mode (:hint nil :exit t)
  "
^Main^
^^^^^^-------------------------------------------------------
_c_: clear-buffer
_o_: clear-output

  "

  ("c" cider-repl-clear-buffer "Clear buffer")
  ("o" cider-repl-clear-output "Clear output")
  ("z" nil "leave"))

(use-package clojure-mode
  :ensure t
  :defer t
  :mode (("\\.clj$" . clojure-mode)
         ("\\.cljs$" . clojure-mode))
  :bind
  (:map clojure-mode-map
        ("C-z" . hydra-cider-mode/body)
        :map cider-repl-mode-map
        ("C-z" . hydra-cider-repl-mode/body))
  :init
  (add-hook 'clojure-mode-hook
            '(lambda ()
               (flycheck-mode t)
               (subword-mode t)
               (smartparens-mode t)
               (rainbow-delimiters-mode t)
               (clj-refactor-mode t)
               (yas-minor-mode t)
               (add-hook 'before-save-hook 'delete-trailing-whitespace)))
  :commands
  (clojure-mode)
  :config
  (use-package clj-refactor
    :ensure t))

(use-package cider
  :ensure t
  :defer t
  :init
  (add-hook 'cider-mode-hook #'clj-refactor-mode)
  (add-hook 'cider-mode-hook #'company-mode)
  (add-hook 'cider-mode-hook #'eldoc-mode)
  (add-hook 'cider-repl-mode-hook #'company-mode)
  (add-hook 'cider-repl-mode-hook #'eldoc-mode)
  :diminish subword-mode
  :config
  (setq nrepl-log-messages t
        cider-repl-display-in-current-window t
        cider-repl-use-clojure-font-lock t
        cider-prompt-save-file-on-load 'always-save
        cider-font-lock-dynamically '(macro core function var)
        cider-overlays-use-font-lock t)
  (cider-repl-toggle-pretty-printing))
