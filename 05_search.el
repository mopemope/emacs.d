;;;
;;; ace-isearch
;;;

(use-package ace-isearch
  :ensure t
  :custom
  (ace-isearch-input-length 6)
  (ace-isearch-jump-delay 1.2)
  (ace-isearch-function 'avy-goto-char)
  (ace-isearch-use-jump 'printing-char)
  :config
  (use-package avy
    :ensure t)
  (use-package swiper
    :ensure t)
  (global-ace-isearch-mode +1))

;;;
;;; anzu
;;;

(use-package anzu
  :ensure t
  :bind
  (("M-%" . anzu-query-replace)
   ("C-M-%" . anzu-query-replace-regexp))
  :custom
  (anzu-mode-lighter "")
  (anzu-search-threshold 1000)
  (anzu-replace-to-string-separator " => ")
  :commands
  (anzu-mode
   anzu-query-replace
   anzu-query-replace-regexp)
  :config
  (global-anzu-mode t))

(use-package symbol-overlay
  :ensure t
  :diminish
  :functions (symbol-overlay-switch-first symbol-overlay-switch-last)
  :commands (symbol-overlay-get-symbol
             symbol-overlay-assoc
             symbol-overlay-get-list
             symbol-overlay-jump-call)
  :bind (("M-i" . symbol-overlay-put)
         ("C-M-n" . symbol-overlay-jump-next)
         ("C-M-m" . symbol-overlay-jump-prev)
         ("M-N" . symbol-overlay-switch-forward)
         ("M-P" . symbol-overlay-switch-backward)
         ("M-C" . symbol-overlay-remove-all))
  :hook ((prog-mode . symbol-overlay-mode))
  :config
  (defun symbol-overlay-switch-first ()
    (interactive)
    (let* ((symbol (symbol-overlay-get-symbol))
           (keyword (symbol-overlay-assoc symbol))
           (a-symbol (car keyword))
           (before (symbol-overlay-get-list a-symbol 'car))
           (count (length before)))
      (symbol-overlay-jump-call 'symbol-overlay-basic-jump (- count))))

  (defun symbol-overlay-switch-last ()
    (interactive)
    (let* ((symbol (symbol-overlay-get-symbol))
           (keyword (symbol-overlay-assoc symbol))
           (a-symbol (car keyword))
           (after (symbol-overlay-get-list a-symbol 'cdr))
           (count (length after)))
      (symbol-overlay-jump-call 'symbol-overlay-basic-jump (- count 1))))

  (bind-keys :map symbol-overlay-map
             ("<" . symbol-overlay-switch-first)
             (">" . symbol-overlay-switch-last)))

(use-package dumb-jump
  :ensure t
  :commands
  (dumb-jump-mode)
  :custom
  (dumb-jump-selector 'ivy))


(use-package jumplist
  :ensure t
  :bind
  (("C-<" . jumplist-previous)
   ("C->" . jumplist-next))
  :custom
  (jumplist-ex-mode t)
  (jumplist-hook-commands '(find-file
                            counsel-find-file
                            counsel-git
                            counsel-git-grep
                            counsel-ag
                            counsel-rg
                            counsel-rg-root
                            smart-jump-go
                            dumb-jump
                            buffer-expose
                            swiper
                            ivy-switch-buffer
                            ivy-resume
                            ivy-done
                            ivy-alt-done
                            isearch-forward
                            isearch-backward
                            end-of-buffer
                            beginning-of-buffer
                            godef-jump
                            meghanada-jump-declaration)))

(use-package smart-jump
  :ensure t
  :config
  (smart-jump-setup-default-registers))
