(use-package cc-mode
  :defer t
  :config
  (use-package google-c-style
    :ensure t
    :init
    (add-hook 'c-mode-common-hook
              (lambda ()
                (google-set-c-style)
                (google-make-newline-indent)))
    :config
    (c-set-offset 'statement-case-open 0))
  (setq indent-tabs-mode nil)
  (setq tab-width 2)
  (setq c-basic-offset 2)
  (rainbow-delimiters-mode t)
  (smartparens-mode t))

(use-package irony
  :ensure t
  :defer t
  :commands (irony-mode)
  :init
  (add-hook 'irony-mode-hook
            (lambda ()
              (setq indent-tabs-mode nil)
              (setq tab-width 2)
              (setq c-basic-offset 2)
              (rainbow-delimiters-mode t)
              (smartparens-mode t)
              (dumb-jump-mode t)
              ;; (add-hook 'after-save-hook
              ;;           'clang-format-buffer)
              (define-key irony-mode-map [remap completion-at-point]
                'irony-completion-at-point-async)
              (define-key irony-mode-map [remap complete-symbol]
                'irony-completion-at-point-async)
              (company-irony-setup-begin-commands)
              (irony-cdb-autosetup-compile-options)))
  (add-hook 'irony-mode-hook 'electric-pair-mode)
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  :config
  (use-package company-irony
    :ensure t
    :config
    (add-to-list 'company-backends '(company-irony :with company-dabbrev-code))
    (setq company-transformers '(company-sort-by-backend-importance))
    (add-to-list 'company-backends 'company-irony))
  (use-package irony-eldoc
    :ensure t)
  (use-package flycheck-irony
    :ensure t)
  (use-package clang-format
    :ensure t)
  ;; (setq clang-format-style-option "google")
  (irony-eldoc)
  (flycheck-irony-setup))
