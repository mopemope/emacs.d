;;;
;;; flycheck-mode
;;;

;;; Code:

(use-package flycheck
  :ensure t
  :defer t
  :bind
  (("M-p" . flycheck-previous-error)
   ("M-n" . flycheck-next-error))
  :commands
  (flycheck-mode flycheck-previous-error flycheck-next-error)
  :config
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(javascript-jshint javascript-jscs)))
  (flycheck-add-mode 'javascript-eslint 'web-mode)
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(json-jsonlist))))

;;;
;;; flymake
;;;

(use-package flymake-diagnostic-at-point
  :ensure t
  :after flymake
  :custom
  (flymake-diagnostic-at-point-timer-delay 0.1)
  (flymake-diagnostic-at-point-error-prefix "☠ ")
  (flymake-diagnostic-at-point-display-diagnostic-function 'flymake-diagnostic-at-point-display-popup)
  :hook
  (flymake-mode . flymake-diagnostic-at-point-mode))

