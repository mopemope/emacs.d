(use-package yaml-mode
  :defer t
  :ensure t
  :config
  (use-package flycheck-yamllint
    :ensure t))
