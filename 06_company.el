;;;
;;; company-mode
;;;

(use-package company
  :ensure t
  :hook
  (after-init . global-company-mode)
  :commands
  (global-company-mode company-mode)
  :custom
  (company-idle-delay 0)
  (company-minimum-prefix-length 2)
  (company-selection-wrap-around t)
  (company-dabbrev-downcase nil)
  :config
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  (define-key company-search-map (kbd "C-n") 'company-select-next)
  (define-key company-search-map (kbd "C-p") 'company-select-previous)
  (define-key company-active-map (kbd "C-s") 'company-filter-candidates)
  (define-key company-active-map (kbd "C-i") 'company-complete-selection))

;;;
;;; eacl
;;;

(use-package eacl
  :ensure t
  :defer t
  :commands
  (eacl-complete-line)
  :bind (("C-c C-c" . eacl-complete-line)))
