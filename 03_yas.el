;;;
;;; yasnippet
;;;

(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :config
  (yas-global-mode 1)
  (setq yas-prompt-functions '(yas-ido-prompt)))
