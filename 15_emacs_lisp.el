;;;
;;; emacs-lisp-mode
;;;

(use-package emacs-lisp-mode
  :defer t
  :hook
  (emacs-lisp-mode . (lambda ()
                       (add-hook 'before-save-hook 'delete-trailing-whitespace)))
  :bind
  (:map emacs-lisp-mode-map
        ("C-M-." . counsel--imenu))
  :commands
  (emacs-lisp-mode)
  :config
  (use-package flycheck-package
    :ensure t)
  (flycheck-package-setup))
