;;;
;;; web-mode
;;;

(use-package web-mode
  :ensure t
  :defer t
  :mode
  (("\\.html\\'" . web-mode)
   ("\\.vue\\'" . web-mode)
   ("\\.jsx\\'" . web-mode))
  :config
  (use-package company-web
    :ensure t
    :defer t)
  (use-package company-tern
    :ensure t
    :defer t)
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (flycheck-mode t)
  (smartparens-mode t)
  (add-to-list 'company-backends '(company-web-html :with company-abbrev))
  (add-hook 'before-save-hook 'delete-trailing-whitespace))
