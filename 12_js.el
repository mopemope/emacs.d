(use-package add-node-modules-path
  :ensure t
  :commands (add-node-modules-path))
;;;
;;; js2-mode
;;;

(use-package js2-mode
  :ensure t
  :defer t
  :init
  (add-hook 'js2-mode-hook
            '(lambda ()
               (add-node-modules-path)
               (lsp)
               (yas-minor-mode)
               (dumb-jump-mode)
               (smartparens-mode t)
               (rainbow-delimiters-mode t)
               (add-hook 'before-save-hook 'delete-trailing-whitespace)))
  :mode
  ("\\.js$" . js2-mode)
  :commands
  (js2-mode)
  :config
  (setq js2-basic-offset 2)
  (font-lock-add-keywords
   'js2-mode `(("\\(function\\) *("
                (0 (progn
                     (compose-region
                      (match-beginning 1)
                      (match-end 1)
                      "\u03bb")
                     nil)))))
    )

;;;
;;; json-mode
;;;

(use-package json-mode
  :ensure t
  :defer t
  :hook
  (json-mode . (lambda ()
                 (setq js-indent-level 2)
                 (setq json-reformat:indent-width 2)
                 (setq tab-width 2)))
  :mode
  ("\\.json$" . json-mode)
  :commands
  (json-mode))

;;;
;;; tern-mode
;;;

;; (use-package tern
;;   :ensure t
;;   :defer t
;;   :commands
;;   (tern-mode)
;;   :config
;;   (setq tern-command '("tern" "--no-port-file"))
;;   (use-package company-tern
;;     :ensure t)
;;   (setq company-tern-property-marker "")
;;   (add-to-list 'company-backends '(company-tern :with company-dabbrev))
;;   (setq company-transformers '(company-sort-by-backend-importance)))

;; (use-package js2-refactor
;;   :ensure t
;;   :defer t
;;   :after js2-mode
;;   :init
;;   (add-hook 'js2-mode-hook 'js2-refactor-mode))


(use-package typescript-mode
  :ensure t
  :defer t
  :mode ("\\.ts$" . typescript-mode)
  :commands (typescript-mode)
  :hook
  (typescript-mode . (lambda ()
                       (setq js-indent-level 2)
                       (setq typescript-indent-level 2)
                       (setq indent-tabs-mode nil)
                       (setq c-basic-offset 2)
                       (setq indent-tabs-mode nil
                             js-indent-level 2)
                       (add-node-modules-path)
                       (lsp)
                       (yas-minor-mode))))

(use-package vue-mode
  :ensure t
  :defer t
  :mode ("\\.vue$" . vue-mode)
  :commands (vue-mode)
  :hook
  (vue-mode . (lambda ()
                (setq js-indent-level 2)
                (setq indent-tabs-mode nil)
                (setq typescript-indent-level 2)
                (setq c-basic-offset 2)
                (setq indent-tabs-mode nil
                      js-indent-level 2)
                (add-node-mo ddules-path)
                (lsp)
                (yas-minor-mode))))

;;;###autoload (autoload 'vue-format-buffer "vue-format" nil t)
;;;###autoload (autoload 'vue-format-on-save-mode "vue-format" nil t)
(reformatter-define vue-format
  :program "prettier"
  :args (list "--parser" "vue")
  :group 'vue
  :lighter 'VueFmt)

;;;###autoload (autoload 'typescript-format-buffer "typescript-format" nil t)
;;;###autoload (autoload 'typescript-format-on-save-mode "typescript-format" nil t)
(reformatter-define typescript-format
  :program "prettier"
  :args (list "--parser" "typescript")
  :group 'typescript
  :lighter 'TSFmt)
