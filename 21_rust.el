
(use-package rustic
  :ensure t
  :defer t
  :hook
  (rustic-mode-hook . (lambda ()
                        (lsp)
                        (yas-minor-mode)))

  :mode
  ("\\.rs$" . rustic-mode)
  :commands
  (rustic-mode)
  :config
  (lsp)
  (setq lsp-rust-rls-command '("rustup" "run" "stable" "rls"))
  (use-package quickrun
    :defer t
    :ensure t))

;;;###autoload (autoload 'rust-format-buffer "rust-format" nil t)
;;;###autoload (autoload 'rust-format-on-save-mode "rust-format" nil t)
(reformatter-define rust-format
  :program "cargo"
  :args (list "fmt" "-q")
  :group 'rust
  :lighter 'RustFmt)
