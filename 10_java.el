(add-to-list 'load-path (expand-file-name "~/repos/github.com/mopemope/meghanada-emacs"))

(defun counsel-rg-root ()
  (interactive)
  (counsel-rg nil (locate-dominating-file default-directory ".git")))

(use-package autodisass-java-bytecode
  :ensure t
  :defer t)

(use-package google-c-style
  :defer t
  :ensure t
  :commands
  (google-set-c-style))

;; (use-package lsp-java
;;   :ensure t
;;   :init
;;   (add-hook 'java-mode-hook
;;             (lambda ()
;;               (lsp)
;;               (google-set-c-style)
;;               (google-make-newline-indent)
;;               (smartparens-mode t)
;;               (rainbow-delimiters-mode t)
;;               ))

(use-package meghanada
  :defer t
  :ensure t
  :init
  (add-hook 'java-mode-hook
            (lambda ()
              (google-set-c-style)
              (google-make-newline-indent)
              (meghanada-mode t)
              (dumb-jump-mode t)
              (smartparens-mode t)
              (rainbow-delimiters-mode t)
              (add-hook
               'before-save-hook
               (lambda ()
                 (meghanada-code-beautify-before-save)))))
  :config
  (setq indent-tabs-mode nil)
  (setq tab-width 2)
  (setq c-basic-offset 2)
  (setq meghanada-server-remote-debug t)
  (setq meghanada-javac-xlint "-Xlint:all,-processing")
  ;; (setq meghanada-server-jvm-option "-Xms128m -Xmx1024m -XX:+UseG1GC -XX:ReservedCodeCacheSize=240m -XX:SoftRefLRUPolicyMSPerMB=50 -Dsun.io.useCanonCaches=false")
  (setq meghanada-server-jvm-option "-Xms128m -Xmx1024m -XX:+UseConcMarkSweepGC -XX:ReservedCodeCacheSize=240m -XX:SoftRefLRUPolicyMSPerMB=50 -Dmeghanada.idle.cache=true -Dmeghanada.idle.cacheinterval=2")
  (setq meghanada-import-static-enable "java.util.Objects,org.junit.Assert")
  (setq meghanada-cache-in-project nil)
  :bind
  (:map meghanada-mode-map
        ("C-S-t" . meghanada-switch-testcase)
        ("C-M-." . counsel-imenu)
        ("M-r" . meghanada-reference)
        ("M-t" . meghanada-typeinfo)
        ("C-z" . hydra-meghanada/body))
  :commands (meghanada-mode))

(defun _exit ()
  (interactive)
  (if (display-graphic-p)
      (delete-frame)
  (exit)))

(defhydra hydra-meghanada (:hint nil :exit t)
"
^Main^
^^^^^^-------------------------------------------------------
_c_: meghanada-compile-file         _m_: meghanada-restart
_C_: meghanada-compile-project      _t_: meghanada-run-task
_o_: meghanada-optimize-import      _j_: meghanada-run-junit-test-case
_s_: meghanada-switch-test-case     _J_: meghanada-run-junit-class
_v_: meghanada-local-variable       _R_: meghanada-run-junit-recent
_i_: meghanada-import-at-point      _r_: meghanada-reference
_I_: meghanada-import-all           _T_: meghanada-typeinfo
_p_: meghanada-show-project

_g_: magit-status
_a_: counsel-rg
_A_: counsel-rg-root
_l_: counsel-git

_q_: exit
"
  ("c" meghanada-compile-file)
  ("C" meghanada-compile-project)
  ("m" meghanada-restart)
  ("o" meghanada-optimize-import)
  ("s" meghanada-switch-test-case)
  ("v" meghanada-local-variable)
  ("i" meghanada-import-at-point)
  ("I" meghanada-import-all)
  ("p" meghanada-show-project)

  ("g" magit-status)
  ("a" counsel-rg)
  ("A" counsel-rg-root)
  ("l" counsel-git)
  ("I" counsel-imenu)
  ("U" ivy-resume)

  ("t" meghanada-run-task)
  ("T" meghanada-typeinfo)
  ("j" meghanada-run-junit-test-case)
  ("J" meghanada-run-junit-class)
  ("r" meghanada-reference)
  ("R" meghanada-run-junit-recent)

  ("q" _exit)
  ("z" nil "leave"))

(defun my-java-ascii-to-native ()
  (interactive)
  (let ((p (point)))
    (when (and buffer-file-name
               (string-ends-with buffer-file-name ".properties" ))
      (erase-buffer)
      (insert (shell-command-to-string (format "native2ascii -encoding UTF-8 -reverse '%s'" buffer-file-name)))
      (goto-char p))))

(defun my-java-native-to-ascii ()
  (interactive)
  (let ((p (point)))
    (when (and buffer-file-name
               (string-ends-with buffer-file-name ".properties" ))
      (erase-buffer)
      (insert (shell-command-to-string (format "native2ascii -encoding UTF-8 '%s'" buffer-file-name)))
      (goto-char p))))

(defalias 'n2a 'my-java-native-to-ascii)
(defalias 'a2n 'my-java-ascii-to-native)

(use-package groovy-mode
  :defer t
  :ensure t
  :commands (groovy-mode)
  :config
  (setq groovy-indent-offset 2)
  :mode (("\\.gradle\\'" . groovy-mode)))
