(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (select-frame frame)
                (setenv "SSH_AUTH_SOCK" "/run/user/1000/gnupg/S.gpg-agent.ssh")
                (use-package color-theme
                  :ensure t
                  :config
                  (when (display-graphic-p)
                    (add-to-list 'default-frame-alist '(font . "Ricty Diminished 14"))
                    (set-face-attribute 'default nil :font "Ricty Diminished 14"))
                  (use-package doom-themes
                    :ensure t
                    :custom
                    (doom-themes-enable-italic t)
                    (doom-themes-enable-bold t)
                    :custom-face
                    (doom-modeline-bar ((t (:background "#6272a4"))))
                    :config
                    (load-theme 'doom-dracula t)
                    (doom-themes-neotree-config)
                    (doom-themes-org-config)))))

  (use-package color-theme
    :ensure t
    :config
    (use-package doom-themes
      :ensure t
      :custom
      (doom-themes-enable-italic t)
      (doom-themes-enable-bold t)
      :custom-face
      (doom-modeline-bar ((t (:background "#6272a4"))))
      :config
      (load-theme 'doom-dracula t)
      (doom-themes-neotree-config)
      (doom-themes-org-config))))

;;;
;;; color-identifiers-mode
;;;

(use-package color-identifiers-mode
  :ensure t
  :hook
  (after-init . global-color-identifiers-mode))

;;;
;;; highlight-indent-guides
;;;

(use-package highlight-indent-guides
  :ensure t
  :diminish
  :hook
  ((prog-mode yaml-mode) . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-auto-enabled t)
  (highlight-indent-guides-responsive t)
  (highlight-indent-guides-method 'character))

;;;
;;; git-gutter
;;;

(use-package git-gutter
  :ensure t
  :custom
  (git-gutter:modified-sign "~")
  (git-gutter:added-sign    "+")
  (git-gutter:deleted-sign  "-")
  :custom-face
  (git-gutter:modified ((t (:background "#f1fa8c"))))
  (git-gutter:added    ((t (:background "#50fa7b"))))
  (git-gutter:deleted  ((t (:background "#ff79c6"))))
  :config
  (global-git-gutter-mode +1))

;;;
;;; doom-modeline
;;;

(use-package doom-modeline
  :ensure t
  :hook
  (after-init . doom-modeline-mode)
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-with-project)
  (doom-modeline-icon t)
  (doom-modeline-major-mode-icon nil)
  (doom-modeline-minor-modes nil)
  (line-number-mode 0)
  (column-number-mode 0)
  (doom-modeline-def-modeline
   'main
   '(bar workspace-number window-number evil-state god-state ryo-modal xah-fly-keys matches buffer-info remote-host buffer-position parrot selection-info)
   '(misc-info persp-name lsp github debug minor-modes input-method major-mode process vcs checker)))

(use-package volatile-highlights
  :ensure t
  :diminish
  :hook
  (after-init . volatile-highlights-mode)
  :custom-face
  (vhl/default-face ((nil (:foreground "#FF3333" :background "#FFCDCD")))))

(use-package beacon
  :ensure t
  :custom
  (beacon-color "pale green")
  (beacon-blink-duration 0.5)
  (beacon-size 80)
  :config
  (beacon-mode 1))

(use-package presentation
  :ensure t)

(use-package fill-column-indicator
  :ensure t
  :hook
  (prog-mode . fci-mode))

(use-package rainbow-delimiters
  :ensure t
  :defer t
  :hook
  (prog-mode . rainbow-delimiters-mode)
  :commands
  (rainbow-delimiters-mode)
  :config
  (custom-set-faces
   '(rainbow-delimiters-depth-1-face ((t (:foreground "#2aa198"))))
   '(rainbow-delimiters-depth-2-face ((t (:foreground "#b58900"))))
   '(rainbow-delimiters-depth-3-face ((t (:foreground "#268bd2"))))
   '(rainbow-delimiters-depth-4-face ((t (:foreground "#dc322f"))))
   '(rainbow-delimiters-depth-5-face ((t (:foreground "#859900"))))
   '(rainbow-delimiters-depth-6-face ((t (:foreground "#268bd2"))))
   '(rainbow-delimiters-depth-7-face ((t (:foreground "#cb4b16"))))
   '(rainbow-delimiters-depth-8-face ((t (:foreground "#d33682"))))
   '(rainbow-delimiters-depth-9-face ((t (:foreground "#839496"))))))
