;;;
;;; sql-mode
;;;

(use-package sql
  :defer t
  :hook
  (sql-mode . (lambda ()
                (add-hook 'before-save-hook 'delete-trailing-whitespace)))
  :commands
  (sql-mode)
  :mode
  ("\\.sql$" . sql-mode)
  :custom
  (sql-indent-offset 2)
  (indent-tabs-mode nil)
  (tab-width 2)
  (c-basic-offset 2)
  (sql-mysql-options '("--prompt=mysql> "))
  :config
  (use-package sql-indent
    :ensure t))
