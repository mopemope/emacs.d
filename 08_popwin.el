
;;;
;;; popwin
;;;

(use-package popwin
  :ensure t
  :defer t
  :bind
  (("C-x C-t" . popwin:popup-last-buffer)
   ("M-m" . popwin:messages))
  :hook
  (after-init . popwin-mode)
  :commands
  (popwin-mode)
  :config
  (popwin-mode 1)
  ;; (setq helm-samewindow nil)
  ;;(setq display-buffer-function 'popwin:display-buffer)
  (setq popwin:special-display-config
        '(("*Completions*" :noselect t :height 0.4)
          ("*tag" :noselect t :regexp t :height 0.4)
          ("*rust-compilation" :regexp t :height 0.4)
          ("helm" :regexp t :height 0.4)
          ("typeinfo" :regexp t :height 0.4)
          ("reference" :regexp t :height 0.4)
          ("*cider-repl" :regexp t :height 0.4)
          ("*cider-error" :regexp t :height 0.4)
          ("*cider-doc" :regexp t :height 0.4)
          ("*cider-apropos" :regexp t :height 0.4))))

;;;
;;; shell-pop
;;;

(use-package shell-pop
  :ensure t
  :defer t
  :bind
  ("C-t" . shell-pop)
  :custom
  (shell-pop-shell-type (quote ("eshell" "*eshell*" (lambda () (eshell)))))
  (shell-pop-universal-key "C-t")
  (shell-pop-window-size 40)
  (shell-pop-full-span t)
  (shell-pop-window-position "bottom")
  :commands
  (shell-pop)
  :config
  (setq eshell-command-aliases-list
        (append
         (list
          (list "o" "xdg-open")))))

(use-package eshell-z
  :ensure t
  :after eshell-mode)

(defun setup-company-eshell-autosuggest ()
  (with-eval-after-load 'company
    (setq-local company-backends '(company-eshell-autosuggest))
    (setq-local company-frontends '(company-preview-frontend))))

(use-package esh-autosuggest
  :ensure t
  :hook (eshell-mode . esh-autosuggest-mode))
