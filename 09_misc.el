;;;
;;; magit
;;;

(use-package magit
  :ensure t
  :defer t
  :commands
  (magit-status magit-dispatch-popup magit-list-repositories)
  :custom
  (magit-repository-directories '(("~/repos/" . 3))))

;;;
;;; reformatter
;;;

(use-package reformatter
  :ensure t)

;;;
;;; smartparens-mode
;;;

(use-package smartparens
  :ensure t
  :defer t
  :commands
  (smartparens-mode smartparens-global-mode)
  :hook
  (prog-mode . smartparens-mode)
  :config
  (use-package smartparens-config))


;;;
;;; paradox
;;;

(use-package paradox
  :ensure t
  :defer t
  :commands
  (paradox-list-packages paradox-enable)
  :custom
  (paradox-github-token t))


;;;
;;; expand-region
;;;

(use-package expand-region
  :ensure t
  :defer t
  :bind
  (("C-@" . er/expand-region)
   ("C-M-@" . er/contract-region))
  :commands
  (er/expand-region er/contract-region))

;;;
;;; aspell
;;;

(use-package ispell
  :ensure t
  :defer t
  :init
  (setq ispell-program-name "aspell")
  :config
  (add-to-list 'ispell-skip-region-alist '("[^\000-\377]+")))

;; add-to-list 'load-path (expand-file-name "~/.emacs.d/emacs-livedown"))

;; (use-package livedown
;;   :defer t
;;   :commands (livedown-kill livedown-preview)
;;   :config
;;   (custom-set-variables
;;    '(livedown-autostart nil)
;;    '(livedown-open t)
;;    '(livedown-port 1337)
;;    '(livedown-browser nil)))

(use-package origami
  :ensure t
  :config
  (global-origami-mode))

;;;
;;; buffer-expose
;;;

(use-package buffer-expose
  :ensure t
  :init (buffer-expose-mode 1))

;;;
;;; imenu-list
;;;

(use-package imenu-list
  :ensure t
  :custom-face
  (imenu-list-entry-face-1 ((t (:foreground "white"))))
  :custom
  (imenu-list-focus-after-activation t)
  (imenu-list-auto-resize nil))
