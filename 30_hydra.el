(defun generate-buffer ()
  (interactive)
  (switch-to-buffer (make-temp-name "temp-")))

(defun counsel-rg-root ()
  (interactive)
  (counsel-rg nil (locate-dominating-file default-directory ".git")))

(defun _exit ()
  (interactive)
  (if (display-graphic-p)
      (delete-frame)
  (exit)))

(use-package hydra
  :ensure t
  :defer t
  :commands
  (hydra-main/body)
  :bind
  (("C-z" . hydra-main/body)
   ("C-c j" . dumb-jump-hydra/body))
  :config
  (defhydra hydra-main (:hint nil :exit t)
"
^Main^                        ^Ivy/Counsel^                     ^Other^
^^^^^^------------------------------------------------------------------------------------------------
_e_: eval-buffer              _a_: counsel-rg                   _r_: anzu-query-replace
_d_: describe-key             _f_: counsel-describe-function    _M_: make-frame-command
_g_: magit-status             _v_: counsel-describe-variable    _R_: quickrun
_p_: paradox-list-packages    _i_: counsel-imenu                _s_: split-window-horizontally
_b_: buffer-expose            _G_: counsel-git                  _0_: delete-window
_I_: imenu-list-smart-toggle  _j_: counsel-git-grep
_t_: generate-tmp-buffer      _A_: counsel-rg-root (project)

_q_: exit
"
  ("e" eval-buffer)
  ("d" describe-key)
  ("g" magit-status)
  ("p" paradox-list-packages)
  ("b" buffer-expose)
  ("I" imenu-list-smart-toggle)
  ("t" generate-buffer)

  ("a" counsel-rg)
  ("f" counsel-describe-function)
  ("v" counsel-describe-variable)
  ("i" counsel-imenu)
  ("G" counsel-git)
  ("j" counsel-git-grep)
  ("A" counsel-rg-root)

  ("r" anzu-query-replace)
  ("M" make-frame-command)
  ("R" quickrun)
  ("s" split-window-horizontally)
  ("0" delete-window)

  ("q" _exit)
  ("z" nil "leave"))

  (defhydra dumb-jump-hydra (:color blue :columns 3)
    "Dumb Jump"
    ("j" dumb-jump-go "Go")
    ("o" dumb-jump-go-other-window "Other window")
    ("e" dumb-jump-go-prefer-external "Go external")
    ("x" dumb-jump-go-prefer-external-other-window "Go external other window")
    ("i" dumb-jump-go-prompt "Prompt")
    ("l" dumb-jump-quick-look "Quick look")
    ("b" dumb-jump-back "Back")))

(defhydra hydra-smerge (:hint nil)
"
^Move^             ^Edit^
^^^^^^~~-----------------------------------
_n_: next          _b_: keep-base
_p_: prev          _c_: keep-current
                 ^^_a_: keep-all
                 ^^_l_: keep-lower
                 ^^_u_: keep-upper
                 ^^_m_: keep-mine
                 ^^_o_: keep-other
                 ^^_r_: resolve
                 ^^_R_: refine
                 ^^_g_: magit-status
                 ^^_S_: smerge-mode

"
  ("n" smerge-next)
  ("p" smerge-prev)

  ("b" smerge-keep-base)
  ("c" smerge-keep-current)
  ("a" smerge-keep-all)
  ("l" smerge-keep-lower)
  ("u" smerge-keep-upper)
  ("m" smerge-keep-mine)
  ("o" smerge-keep-other)
  ("r" smerge-resolve)
  ("R" smerge-refine)
  ("g" magit-status)
  ("S" smerge-mode)

  ("z" nil "leave"))

(use-package smerge-mode
  :defer t
  :commands
  (smerge-mode)
  :bind
  (:map smerge-mode-map
        ("C-z" .  hydra-smerge/body)))
