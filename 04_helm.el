;;;
;;; helm
;;;

;; (use-package helm
;;   :ensure t
;;   :defer t
;;   :commands
;;   (helm-mode
;;    helm-config
;;    helm-ls-git-ls
;;    helm-ag
;;    helm-ag-project-root
;;    helm-do-ag
;;    helm-do-ag-project-root
;;    helm-imenu
;;    helm-mini
;;    helm-M-x
;;    helm-show-kill-ring
;;    helm-find-files
;;    helm-for-files
;;    imenu)
;;   :bind
;;   (("C-x C-f" . helm-find-files)
;;    ("C-x b" . helm-for-files)
;;    ("M-x" . helm-M-x)
;;    ("C-:" . helm-mini)
;;    ("C-," . helm-mini)
;;    ("M-y" . helm-show-kill-ring)
;;    ("C-x C-c" . helm-M-x)
;;    ("C-;" . helm-resume)
;;    ("C-." . helm-resume))
;;   :config
;;   (use-package helm
;;     :ensure t)
;;   (use-package helm-ls-git
;;     :ensure t)
;;   (use-package helm-ag
;;     :ensure t)
;;   (use-package helm-config)
;;   (helm-mode t)

;;   (define-key helm-find-files-map (kbd "C-h") 'delete-backward-char)

;;   (defvar helm-source-emacs-commands
;;     (helm-build-sync-source "Emacs commands"
;;       :candidates (lambda ()
;;                     (let ((cmds))
;;                       (mapatoms
;;                        (lambda (elt) (when (commandp elt) (push elt cmds))))
;;                       cmds))
;;       :coerce #'intern-soft
;;       :action #'command-execute)
;;     "A simple helm source for Emacs commands.")

;;   (defvar helm-source-emacs-commands-history
;;     (helm-build-sync-source "Emacs commands history"
;;       :candidates (lambda ()
;;                     (let ((cmds))
;;                       (dolist (elem extended-command-history)
;;                         (push (intern elem) cmds))
;;                       cmds))
;;       :coerce #'intern-soft
;;       :action #'command-execute)
;;     "Emacs commands history")
;;   (custom-set-variables
;;    '(helm-truncate-lines t)
;;    '(helm-delete-minibuffer-contents-from-point t)
;;    '(helm-mini-default-sources '(helm-source-buffers-list
;;                                  helm-source-files-in-current-dir
;;                                  helm-source-ls-git
;;                                  helm-source-recentf))
;;    '(helm-ag-base-command "rg --no-heading")))

;; ;;;
;; ;;; helm-swoop
;; ;;;

;; (use-package helm-swoop
;;   :defer t
;;   :ensure t
;;   :commands
;;   (helm-swoop
;;    helm-swoop-back-to-last-point
;;    helm-multi-swoop
;;    helm-multi-swoop-all)
;;   :bind
;;   (("M-i" . helm-swoop)
;;    ("M-I" . helm-swoop-back-to-last-point)
;;    ("C-c M-i" . helm-multi-swoop)
;;    ("C-x M-i" . helm-multi-swoop-all))
;;   :config
;;   (use-package helm
;;     :ensure t)
;;   (setq helm-swoop-speed-or-color t)
;;   (setq helm-swoop-split-direction 'split-window-vertically)
;;   (setq helm-swoop-split-with-multiple-windows nil)
;;   (setq helm-multi-swoop-edit-save t))
