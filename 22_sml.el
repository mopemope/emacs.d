(use-package sml-mode
  :defer t
  :ensure t
  :mode ("\\.sml$" . sml-mode))
