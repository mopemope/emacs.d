;;;
;;; lfe-mode
;;;

(use-package lfe-mode
  :ensure t
  :defer t
  :init
  (add-hook 'lfe-mode-hook
            '(lambda ()
               (flycheck-mode t)
               (smartparens-mode t)
               (rainbow-delimiters-mode t)))

  :mode ("\\.lfe$" . lfe-mode)
  :commands (lfe-mode))
