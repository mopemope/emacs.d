(use-package csharp-mode
  :defer t
  :ensure t
  :mode
  ("\\.cs$" . csharp-mode)
  :init
  (add-hook 'csharp-mode-hook
            (lambda ()
              (omnisharp-mode t)
              (dumb-jump-mode t)
              (smartparens-mode t)
              (rainbow-delimiters-mode t)
              (add-hook 'before-save-hook
                        'delete-trailing-whitespace)))
  :config
  (use-package company
    :ensure t)
  (use-package omnisharp
    :ensure t)
  (setq indent-tabs-mode nil)
  (setq c-syntactic-indentation t)
  (electric-pair-local-mode 1)
  (flycheck-mode t)
  (add-to-list 'company-backends 'company-omnisharp)
  )
